def hello_my_friend(arr)
	if arr[2].to_i > 0 and arr[2].to_i < 18
		puts "Привет, #{arr[0]} #{arr[1]}. Тебе меньше 18 лет, но начать учиться программировать никогда не рано"
	elsif arr[2].to_i >= 18
		puts "Привет, #{arr[0]} #{arr[1]}. Самое время заняться делом!"
	else
		puts "Вы ввели некоректный возраст"
	end
end

def broken_sum(arr)
	if arr[0].to_i == 20 or arr[1].to_i == 20 then puts 20
	else puts arr[0].to_i + arr[1].to_i
	end
end

if ARGV.count == 3 then hello_my_friend(ARGV)
elsif ARGV.count == 2 then broken_sum(ARGV)
else abort "введите 2 или 3 аргумента, в зависимости от необходимого скрипта"
end