#проверка количества аргументов
def check_args
	if ARGV.count == 0 then abort "вы не ввели аргумент"
	elsif ARGV.count > 1 then abort "аргументов слишком много, нужен только один"
	end
end

#задание 1
def stringonator(arg)
	if arg[-2..-1] == "CS" then puts 2 ** arg.size
	else puts arg.reverse
	end
end

#задание 2
def release_pocemons(arg)

	if arg <= 0 then abort "не вводите 0, или отрицательные числа" end
	
	pokemons = []

	phrases = ["Введите имя", "Введите цвет", "Покемон сохранен"]	
	
	arg.times do
		puts phrases[0]
		name = STDIN.gets.chomp
		puts phrases[1]
		color = STDIN.gets.chomp
		pokemons << {name: name, color: color}
		puts phrases[2]
		puts ""
	end	
	puts pokemons
end

#далее начинаеться блок непосредственного выполнения программы
check_args

if ARGV[0] != "0" and ARGV[0].to_i == 0
	stringonator(ARGV[0])
elsif ARGV[0].to_i > 0 
	release_pocemons(ARGV[0].to_i)
else 
    abort "введен некорректный параметр"
end
