$file_data = []

#запрос у пользователья имя существующего файла
def setup_filename
	filename = ""
	puts "Введите имя импортируемого файла"
	while true
		filename = gets.chomp
		#если файл существует - возвращает его имя, 
		#если нет - продолжает бесконечный цикл опроса имени файла
		if  File.exist?(filename) then return filename end
		puts "Такого файла не существует, попробуйте еще раз"
	end
end

#запрос у пользователя возраста для вывода из файла
def setup_age
	puts "Введите возвраст"
	#валидация по значениям не производиться
	age = gets.chomp
	#если возраст -1 - выходит из программы
	if age == "-1" then end_of_program end
	return age
end

#возвращает содержимое файла с именем, получаемым из setup_filename
def get_file_data
	return File.read(setup_filename).split("\n")
end

#функция принимает возраст
def find_line_with_age(age)
	#ищет первое совпадение по возрасту из массива значений из файла
	for line in $file_data
		#при совпадении
		if line.chomp.split(" ")[2].to_i == age.to_i then
			#выводит строку на экран
			puts line
			#дописывает строку в файл ответа
			File.write("result.txt", line.to_s + "\n", mode: "a")
			#удаляет строку из массива данных из файла
			$file_data.delete(line)
			#для проверки корректности (и удобства использования) - выводит что в файле осталось
			puts "log: " + $file_data.to_s
		end
	end
	#З.Ы. у функции слишком много ответственности и в целом её хорошо бы разбить, но так как я в руби не очень, то оставлю так
end

#выход из программы
def end_of_program
	#выводит файл ответа
	File.foreach("result.txt") {
		|line|
		puts line
	}
	#выходит без сообщения
	abort
end

#инициализатор программы
def best_program_ever
	#загружает в глобальную переменную массив значений, хранящихся в файле
	$file_data = get_file_data
	#работает пока массив не опустеет (неявно: или пока из программы не выйдут в ручную введа возраст -1)
	while $file_data != []
		find_line_with_age(setup_age)	
	end
	#(если не вышли в ручную) выход из программы
	end_of_program
end

#непосредственно инициализация
best_program_ever
