$user_balance = 0
$balance_in_file = false

#установка баланса в глобальную перменную
def set_user_balance
	#если существует файл с балансом - установить значение из него
	if File.exist?("balance.txt") then
		$user_balance = File.read("balance.txt").to_i
		$balance_in_file = true
	#иначе установить в 100
	else 
		$user_balance = 100
		$balance_in_file = false
	end
end

#перезапись баланса в файл
def rewrite_user_balance_file
	#если данные об балансе были прочитаны из файла
	if $balance_in_file then
		#если файл существует - перезаписывает
		if File.exist?("balance.txt") then
			File.write("balance.txt", $user_balance, mode: "w")
		#если файл удалили/переименовали/перенесли
		else
			puts "Ошибка записи - файл баланса не найден"
		end
	end
	#иначе нам ничего делать не надо
end

#вывод текста меню
def get_choise_menu
	puts "Выберите действие"
	puts "D - положить"
	puts "W - снять"
	puts "B - проверить баланс"
	puts "Q - выйти\n"
end

#спрашивает необходимый пункт меню и переводит в нижний регистр
def ask_choise
	return gets.chomp.downcase
end

#принимает имя операции над финансами
def ask_number_for(for_what)
	puts "Введите ссуму котрую вы хотите #{for_what}"
	temp = gets.to_i
	#проверка на неотрицательность с повторным вводом при ошибке
	while temp < 0
		puts "Не вводите отрицательные числа\nПопробуйте еще раз"
		temp = gets.to_i
	end
	return temp
end

#положить на счет
def deposit
	$user_balance += ask_number_for("положить")
	balance
end

#снять со счета
def withdraw
	temp = ask_number_for("снять")
	#проверка на достаточность денег на счету
	if $user_balance - temp >= 0 then 
		$user_balance -= temp
	else
		puts "Недостаточно средств"
	end
	balance
end

#вывод баланса
def balance
	puts "Ваш баланс равен #{$user_balance}\n"
end

#текст выхода из приложения для меню
def quit
	puts "Спасибо что пользовались нашим приложением\n"
end	

#текст ошибки ввода команды для меню
def wrong_paramether
	puts "Вы указали неверную комманду, попробуйте еще раз"
end

#пользовательский интерфейс
def ui
	again = true
	while again
		get_choise_menu
		user_choise = ask_choise
		if user_choise == "d" then
			deposit
		elsif user_choise == "w" then
			withdraw
		elsif user_choise == "b" then
			balance
		elsif user_choise == "q" then
			again = false
			quit
		else 
			wrong_paramether
		end
	end
end

#собственно программа
set_user_balance
ui
rewrite_user_balance_file
