class CashMachine

	def initialize
		@user_balance = 0	
	end
	
	def get_choise_menu
		puts "Выберите действие"
		puts "D - положить"
		puts "W - снять"
		puts "B - проверить баланс"
		puts "Q - выйти\n"
	end

	def ask_choise
		return gets.chomp.downcase
	end

	def ask_number_for(for_what)
		puts "Введите ссуму котрую вы хотите #{for_what}"
		temp = gets.to_i
		while temp < 0
			puts "Не вводите отрицательные числа\nПопробуйте еще раз"
			temp = gets.to_i
		end
		return temp
	end

	def deposit
		@user_balance += ask_number_for("положить")
		balance
	end

	def withdraw
		temp = ask_number_for("снять")
		if @user_balance - temp >= 0 then 
			@user_balance -= temp
		else
			puts "Недостаточно средств"
		end
		balance
	end
	
	def balance
		puts "Ваш баланс равен #{@user_balance}\n"
	end

	def quit
		puts "Спасибо что пользовались нашим приложением\n"
	end	

	def wrong_paramether
		puts "Вы указали неверную комманду, попробуйте еще раз"
	end

	def ui
		again = true
		while again
			get_choise_menu
			user_choise = ask_choise
			if user_choise == "d" then
				deposit
			elsif user_choise == "w" then
				withdraw
			elsif user_choise == "b" then
				balance
			elsif user_choise == "q" then
				again = false
				quit
			else 
				wrong_paramether
			end
		end
	end
	
	def read_user_balance
		if File.exist?("balance.txt") then
			@user_balance = File.read("balance.txt").to_i
		else 
			@user_balance = 100
		end
	end

	def rewrite_user_balance_file
		if File.exist?("balance.txt") then
			File.write("balance.txt", @user_balance, mode: "w")
		end
	end

	def start
		read_user_balance
		ui
		rewrite_user_balance_file
	end
	
	def self.init
		CashMachine.new.start
	end

end

CashMachine.init
