#класс-представление поста
class Post
  attr_accessor :content, :comments
  def initialize(content)
    #непосредственно контент
    @content = content
    #комментарии к посту
    @comments = []
  end
end

#как я понял, это - модуль с ЮИ для контроллеров
module Resource
  def connection(routes)
    if routes.nil?
      puts "No route matches for #{self}"
      return
    end

    loop do
      print 'Choose verb to interact with resources (GET/POST/PUT/DELETE) / q to exit: '
      verb = gets.chomp
      break if verb == 'q'

      #добавим валидацию
      if verb != "GET" and verb != "POST" and verb != "PUT" and verb != "DELETE" and verb != "q" then
        puts "Exception: wrong argument"
        next
      end

      action = nil

      if verb == 'GET'
        print 'Choose action (index/show) / q to exit: '
        action = gets.chomp
        break if action == 'q'
      end

      action.nil? ? routes[verb].call : routes[verb][action].call
    end
  end
end

#контроллер постов
class PostsController
  extend Resource
  
  #массив с постами берем извне для связи с контроллером комментов
  def initialize(posts)
    @posts = posts
  end
  
  #валидируем индекс на:
  def index_validate(index, array)
    #то что введен без стринговских "примесей"
    if index != index.to_i.to_s then
      puts "Exception: #{index} is not ineger"
      return -1
    end
    #то что индекс не выходит за пределы массива постов
    if index.to_i < 0 or index.to_i > array.size - 1 then
      puts "Exception: index out of bounds"
      return -1
    end

    return 0
    
  end

  #вывод всех постов и их индексы
  def index
    for i in 0..(@posts.size-1)
    	puts i.to_s + " " + @posts[i].content
    end
  end

  #вывод поста с конкретным индексом
  def show

    print 'Choose index of post: '
    index = gets.chomp
    
    if self.index_validate(index, @posts) == -1 then return end
    
    puts @posts[index.to_i].content
    
  end
  
  #создать пост
  def create
    puts "Enter your post's content:"
    content = gets.chomp
    @posts.push(Post.new(content))
  end
  #перезаписать пост с определенным индексом
  def update
    print 'Choose index of post: '
    index = gets.chomp
    
    if self.index_validate(index, @posts) == -1 then return end
    
    puts "Rewrite your post's content:"
    content = gets.chomp
    @posts[index.to_i].content = content
    
  end

  #удалить пост с определенным индексом
  def destroy
    print 'Choose index of post: '
    index = gets.chomp
    
    if self.index_validate(index, @posts) == -1 then return end
    
    @posts.delete_at(index.to_i)
    
  end
end

#контроллер комментов
#примечание: так как в задании не говориться что он вообще должен делать - пришлось фантазировать
class CommentsController
  extend Resource
  
  #так как комменты должны быть привязаны к постам то:
  #посты были определенны как класс в котором помимо контетна содержаться и комменты к нему
  #для синхронизации работы (так как у нас нет сервиса по выдачи постов) в контроллеры постов и комментов передается ссылка на один и тот же массив
  def initialize(posts)
    @posts = posts
  end
  
  #валидация как у контроллера постов, вообще можно было бы её передать родителю, но он про индексы ничего не знает
  #конечно можно было бы попытаться для этого методы index, show и так далее выполнить в родителе как "шаблонный метод", но не уверен на сколько
  #это актуально в языках с гибкой типизацией, потому оставил так
  def index_validate(index, array)
    
    if index != index.to_i.to_s then
      puts "Exception: #{index} is not ineger"
      return -1
    end
    
    if index.to_i < 0 or index.to_i > array.size - 1 then
      puts "Exception: index out of bounds"
      return -1
    end
    
    return 0
    
  end
  
  #этот метод был отправной точкой к рефакторингу постов в классы, ибо у руби массивы динамические и индексы при удалении постов "плывут"
  #и так как комменты без поста ничего не стоят, то в этом методе для всех постов выводится индекс поста, его контент, и комменты со своими индексами
  def index
    for i in 0..(@posts.size-1)
      puts "post index: #{i}"
      puts "post contnt:\n" + @posts[i].content
      puts "comments: "
      for j in 0..(@posts[i].comments.size - 1)
        puts j.to_s + " " + @posts[i].comments[j]
      end
    end
  end

  #вывод для поста с индексом i коммента с индексом j
  def show

    print 'Choose index of post: '
    post_index = gets.chomp
    
    if self.index_validate(post_index, @posts) == -1 then return end
    
    print 'Choose index of comment: '
    comment_index = gets.chomp
    
    if self.index_validate(comment_index, @posts[post_index.to_i].comments) == -1 then return end
    
    puts @posts[post_index.to_i].comments[comment_index.to_i]
    
  end
  
  #создание коммента в посте с индексом i 
  def create
    print "Choose index of the post for which you are writing a comment: "
    index = gets.chomp
    
    if self.index_validate(index, @posts) == -1 then return end
    
    puts "Enter your comment:"
    
    comment = gets.chomp
    
    @posts[index.to_i].comments.push(comment)
  end

  #обновление коммента с индексом j для поста с индексом i
  def update
    print 'Choose index of post: '
    post_index = gets.chomp
    
    if self.index_validate(post_index, @posts) == -1 then return end
    
    print 'Choose index of comment: '
    comment_index = gets.chomp
    
    if self.index_validate(comment_index, @posts[post_index.to_i].comments) == -1 then return end
    
    puts "Rewrite comment:"
    comment = gets.chomp
    @posts[post_index.to_i].comments[comment_index.to_i] = comment
    
  end
  
  #удаление коммента с индексом j для поста с индексом i
  def destroy
    print 'Choose index of post: '
    post_index = gets.chomp
    
    if self.index_validate(post_index, @posts) == -1 then return end
    
    print 'Choose index of comment: '
    comment_index = gets.chomp
    
    if self.index_validate(comment_index, @posts[post_index.to_i].comments) == -1 then return end
    
    @posts[post_index.to_i].comments.delete_at(comment_index.to_i)
    
  end
end

class Router
  def initialize
    @routes = {}
  end

  def init
  
    #тут был рефактор: 
    #массив постов создается вне контроллеров
    posts = []
  
    #потом массив передается методам создания контроллеров по классу
    resources(PostsController, 'posts', posts)
    resources(CommentsController,'comments', posts)

    loop do
      print 'Choose resource you want to interact (1 - Posts, 2 - Comments, q - Exit): '
      choise = gets.chomp

      PostsController.connection(@routes['posts']) if choise == '1'
      CommentsController.connection(@routes['comments']) if choise == '2'
      break if choise == 'q'
    end

    puts 'Good bye!'
  end
  
  #метод создания контроллеров принимает массив постов
  def resources(klass, keyword, posts_arr)
    #и передает его контруктору контроллера (таким образом оба контроллера обращаются к одному массиву постов)
    controller = klass.new(posts_arr)
    @routes[keyword] = {
      'GET' => {
        'index' => controller.method(:index),
        'show' => controller.method(:show)
      },
      'POST' => controller.method(:create),
      'PUT' => controller.method(:update),
      'DELETE' => controller.method(:destroy)
    }
    
  end
end

router = Router.new

router.init
